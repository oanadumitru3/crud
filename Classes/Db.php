<?php

class Db
{
    private $connection;

    function __construct()
    {
        $this->connect_db();
    }

    public function connect_db(){
        $this->connection = mysqli_connect('45.15.23.59','root','Sco@l@it123','national-03-ioana');
        if (mysqli_connect_error()){
            die("Database connection failed".mysqli_connect_error().mysqli_connect_errno());
        }
    }

    public function insertRecord($post){
        $name = $post['name'];
        $description = $post['description'];
        $days = $post['days'];
        $price = $post['price'];
        $persons = $post['persons'];
        $sql = "INSERT INTO reservations(name,description,days,price,persons)VALUES('$name','$description','$days','$price','$persons')";
        $result = $this->connection->query($sql);
        if ($result){
            header("location:reservation.php");
        }else{
            echo "Error".$this->connection->error;

        }
    }

    public function updateRecord($post){
        $name = $post['name'];
        $description = $post['description'];
        $days = $post['days'];
        $price = $post['price'];
        $persons = $post['persons'];
        $editid = $post['hid'];
        $sql = "UPDATE reservations SET name='$name',description='$description',days='$days',price='$price',persons='$persons' WHERE id='$editid'";
        $result = $this->connection->query($sql);
        if ($result){
            header("location:reservation.php");
        }else{
            echo "Error".$this->connection->error;

        }
    }

    public function displayRecord(){
        $sql = "SELECT * FROM reservations";
        $result = $this->connection->query($sql);
        if ($result->num_rows>0){
            while ($row = $result->fetch_assoc()){
                $data[] = $row;
            }
            return $data;
        }
    }


    public function displayRecordById($editid){
        $sql = "SELECT * FROM reservations WHERE id = '$editid'";
        $result = $this->connection->query($sql);
        if ($result->num_rows==1){
            $row = $result->fetch_assoc();
            return $row;
        }
}


    public function deleteRecord($id){
        $sql = "DELETE FROM reservations WHERE id ='$id'";
        $result = $this->connection->query($sql);
        if ($result){
            header('location:reservation.php');
        }else{
            echo "Error";
        }
    }

}

$Db = new Db();
