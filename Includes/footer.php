<div class="container my-5">
    <footer class="text-center text-lg-start" style="background-color:indianred;">
        <div class="container d-flex justify-content-center py-5">
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2"
                    style="background-color: darkred">
                <i class="fab fa-facebook-f"></i>
            </button>
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2"
                    style="background-color: darkred">
                <i class="fa-brands fa-instagram"></i>
            </button>
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2"
                    style="background-color: darkred">
                <i class="fa-brands fa-youtube"></i>
            </button>
        </div>
        <div class="text-center text-white p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2022 Copyright:
            <a class="text-white" href="#">blabla.com</a>
        </div>
    </footer>
</div>
