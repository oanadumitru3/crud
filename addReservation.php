<?php
include "Includes/head.php";
include "Includes/header.php";
include "Classes/Db.php";

if (isset($_POST['submit'])){
    $Db->insertRecord($_POST);
}
?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Add reservation!</h2>
            <form action="addReservation.php" method="post">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1" style="width:10%;">Destination </span>
                    <input name="name" type="text" class="form-control" placeholder="" aria-label="destination" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1" style="width:10%;">Description</span>
                    <input name="description" type="text" class="form-control" placeholder="" aria-label="describtion" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1" style="width:10%;">Days</span>
                    <input name="days" type="text" class="form-control" placeholder="" aria-label="days" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1" style="width:10%;">Price </span>
                    <input name="price" type="text" class="form-control" placeholder="" aria-label="price" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1" style="width:10%;">Persons</span>
                    <input name="persons" type="text" class="form-control" placeholder="" aria-label="persons" aria-describedby="basic-addon1">
                </div>
                <button type="submit" name="submit" class="btn btn-danger" style="width: 50%;" >Save</button>
                <button  type="submit" class="btn btn-secondary" style="width: 50%;"> Cancel</button>
            </form>
        </div>
    </div>
</div>

<?php
include "Includes/footer.php";
?>

</body>
</html>
