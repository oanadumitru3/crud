<?php
include "Includes/head.php";
include "Includes/header.php";
include "Classes/Db.php";

if (isset($_GET['deleteid'])){
    $deleteid = $_GET['deleteid'];
    $Db->deleteRecord($deleteid);
}

?>

<div class="container">
    <div class="row">
        <div class ="col-12">
            <div class="alert alert-danger">
                Are you sure you want to delete the reservation?
            </div>
            <a href="updateReservation.php?deleteid=<?php echo $deleteid; ?>" type="button" class="btn btn-danger">Yes </a>
            <a href="reservation.php" type="button" class="btn btn-danger">NO </a>
        </div>
    </div>
</div>
</body>
</html>
