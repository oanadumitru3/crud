
<?php
include "Includes/head.php";
include "Includes/header.php";
include "Classes/Db.php";

?>

<a href="addReservation.php" type="button" class="btn btn-primary btn-lg" style="background-color:indianred"> Add reservation</a>
<hr>

<?php
$data = $Db->displayRecord();
foreach ($data as $datum){
?>

<div class="container">
    <section class="mx-auto my-5" style="max-width: 23rem;">
        <div class="card">
            <div class="card-body d-flex flex-row">
                <div>
                    <h5 class="card-title font-weight-bold mb-2"><a href="viewReservation.php?id=<?php echo $datum['id']; ?>"><?php echo $datum['name']; ?></h5>
                </div>
            </div>
            <div class="bg-image hover-overlay ripple rounded-0" data-mdb-ripple-color="light">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/full page/2.jpg"
                     alt="Card image cap" />
                <a href="#!">
                    <div class="mask" style="background-color: black;"></div>
                </a>
            </div>
            <div class="card-body"><?php echo $datum['description'] ?></div>
            </div>
        </div>


<?php }
include "Includes/footer.php";?>

</body>
</html>
