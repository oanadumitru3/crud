<?php
include "Classes/Db.php";
include "Includes/head.php";
include "Includes/header.php";

$Db = new Db();
if (isset($_GET['editid'])) {
    $editid = $_GET['editid'];
    $myrecord = $Db->displayRecordById($editid);
}

if (isset($_POST['update'])){
    $Db->updateRecord($_POST);
     }

?>

<div class="container">
    <div class="row">
        <div class ="col-12">
            <form method="post" class="form-horizontal col-md-6 col-md-offset-3">
                <h2>Edit reservation</h2>
                <div class="form-group">
                    <label for="input1" class="col-sm-2 control-label">Destination</label>
                    <div class="col-sm-10">
                        <input type="text" name="name"  class="form-control" id="input1" value="<?php echo $myrecord['name'] ?>" style="width: 290%;"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="input1" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <input type="text" name="description"  class="form-control" id="input1" value="<?php echo $myrecord['description'] ?>" style="width: 290%;"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="input1" class="col-sm-2 control-label">Days</label>
                    <div class="col-sm-10">
                        <input type="text" name="days"  class="form-control" id="input1" value="<?php echo $myrecord['days']  ?>" style="width: 290%;" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="input1" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="text" name="price"  class="form-control" id="input1" value="<?php echo $myrecord['price'] ?>" style="width: 290%;" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="input1" class="col-sm-2 control-label">Persons</label>
                    <div class="col-sm-10">
                        <input type="text" name="persons"  class="form-control" id="input1" value="<?php echo $myrecord['persons'] ?>" style="width: 290%;" />
                    </div>
                </div>

    <div class="form-group">
        <input type="hidden" name="hid" value="<?php echo $myrecord['id']?>">
        <input type="submit" name="update" value="Update" class="btn btn-danger">
    </div>
            </form>
        </div>
    </div>
</div>
<?php
include "Includes/footer.php";
?>
</body>
</html>

