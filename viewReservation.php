<?php
include "Includes/head.php";
include "Includes/header.php";
include "Classes/Db.php";

$id = $_GET['id'];
$myrecord = $Db->displayRecordById($id);

?>

<article class="postcard light yellow" style="text-align: center">
    <h1 class="postcard__title yellow"><a><?php echo $myrecord['name']; ?></a></h1>
    <a class="postcard__img_link">
        <img class="postcard__img" src="https://picsum.photos/501/501" alt="Image Title"/>
    </a>
    <div class="postcard__text t-dark">
        <div class="postcard__subtitle small">
            <time datetime="2020-05-25 12:00:00">
                <i class="fas fa-calendar-alt mr-2"></i>Mon, July 25th 2023
            </time>
        </div>
        <div class="postcard__bar"></div>
        <div class="postcard__preview-txt"><?php echo $myrecord['description'] ?></div>
        <ul style="margin: 0 auto; display: table">
            <li><a><i class="fa-solid fa-calendar-days"></i> <?php echo $myrecord['days']; ?> days.</a></li>
            <li><a><i class="fa-solid fa-person"></i> <?php echo $myrecord['persons']; ?> persons.</a></li>
            <li><a><i class="fa-solid fa-money-bill-1-wave"></i> <?php echo $myrecord['price'] ?> Ron.</a></li>
        </ul>
    </div>

</article>
<hr>
<a href="updateReservation.php?editid=<?php echo $myrecord['id']; ?>"  type="button" class="btn btn-danger"  name="update" >Edit</a>
<a href="deleteReservation.php?deleteid=<?php echo $myrecord['id']; ?>"  type="button" class="btn btn-danger"  name="delete" >Delete</a>
<a href="reservation.php" type="button" class="btn btn-secondary" > Cancel</a>


<?php
include "Includes/footer.php";
?>
</body>
</html>
